# Shaker

This project is created to solve the task of anonymising data of `customer` table into separate `customer_anonymised` table. Tech stack:

- Express
- MongoDB
- Apollo GQL
- TypeScript

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js and npm installed
- MongoDB is installed

## Installation

1. Install TypeScript globally:

   ```bash
   npm i -g typescript
   ```

2. Install project dependencies:

   ```bash
   npm i
   ```

3. Setup environment

   Create `.env` file and save db uri:

   ```
   DB_URI = '<YOUR_DB_URI>'
   ```

## Introduction

There are 3 main points that you should know before start development:

1.  Server uses `3500` port
2.  To access data we use GQL calls to `/graphql` endpoint, e.g:

    ```gql
    query GetCustomers {
        getCustomers {
            _id
            firstName
            lastName
            email
            createdAt
        }
    }
    ```

3.  To use GQL subscriptions use `/subscriptions` endpoint, e.g:

    ```gql
    subscription AnonCustomers {
        anonCustomers {
            _id
            firstName
            lastName
            email
            createdAt
        }
    }
    ```

## Development

To start the project in development mode, run:

```bash
npm run dev
```

## Linting

To start linter, run:

```bash
npm run lint
or
npm run lint:fix    --> this will run lint with --fix flag
```

## Building

To build the project, run:

```bash
npm run build
```

## Start

To start the project in production, run:

```bash
npm run start
```
