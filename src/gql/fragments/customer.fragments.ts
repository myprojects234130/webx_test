export const CustomerFragments = `
  type CustomerAddress {
	line1: String
	line2: String
	postcode: String
	city: String
	state: String
	country: String
  }

  type Customer {
	_id: String
	firstName: String
	lastName: String
	email: String
	address: CustomerAddress
	createdAt: String
  }
`;
