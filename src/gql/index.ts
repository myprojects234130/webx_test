import { CustomerFragments } from './fragments';
import { CustomerResolver } from './resolvers';

export const typeDefs = `
	${CustomerFragments}

  type Query {
	  getCustomers: [Customer]
  }

  type Subscription {
	  anonCustomers: [Customer]
  }
`;

export const resolvers = {
  Query: {
    ...CustomerResolver.Query,
  },
  Subscription: {
    ...CustomerResolver.Subscription,
  },
};
