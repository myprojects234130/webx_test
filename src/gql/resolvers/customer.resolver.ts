import { Trigger } from '@gql/subscriptions/enums';
import { pubSub } from '@gql/subscriptions';
import { customersService } from '@modules/customers';

export const CustomerResolver = {
  Query: {
    getCustomers: () => customersService.getAll(),
  },
  Mutation: {},
  Subscription: {
    anonCustomers: {
      subscribe: () => pubSub.asyncIterator(Trigger.CUSTOMERS_ANONYMISED),
    },
  },
};
