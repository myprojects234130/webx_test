import express, { Application, json } from 'express';
import { ApolloServer } from '@apollo/server';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import { expressMiddleware } from '@apollo/server/express4';
import { makeExecutableSchema } from '@graphql-tools/schema';
import cors from 'cors';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import { createServer } from 'http';
import 'dotenv/config';

import { resolvers, typeDefs } from '@gql';
import { initDb } from '@db';
import { fakeInsert } from '@utils';
import { customerAnonymisedService } from '@modules/customers-anonymised';

const app: Application = express();
const httpServer = createServer(app);

const schema = makeExecutableSchema({ typeDefs, resolvers });

const wsServer = new WebSocketServer({
  server: httpServer,
  path: '/subscriptions',
});

const serverCleanup = useServer({ schema }, wsServer);

const server = new ApolloServer({
  schema,
  plugins: [
    ApolloServerPluginDrainHttpServer({ httpServer }),
    {
      async serverWillStart() {
        return {
          async drainServer() {
            await serverCleanup.dispose();
          },
        };
      },
    },
  ],
});
await server.start();

app.use(cors(), json());
app.use('/graphql', expressMiddleware(server));

(async () => {
  await initDb();

  let intervalId: NodeJS.Timeout;
  httpServer.listen(3500, () => {
    customerAnonymisedService.listen();
    intervalId = setInterval(() => fakeInsert(), 200);
    console.log('Application is running on port 3500!');
  });

  process.on('exit', () => {
    console.log('Server is closing. Clearing the interval...');
    clearInterval(intervalId);
  });
})();
