import { faker } from '@faker-js/faker';
import { customersService } from '@modules/customers';

export function fakeInsert() {
  const count = faker.number.int({ min: 1, max: 10 });
  for (let i = 0; i <= count; i++) {
    customersService.insert(customersService.generate());
  }
}
