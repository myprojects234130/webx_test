import { Db, Document, MongoClient, ServerApiVersion } from 'mongodb';
import { Collection } from './enums';
import { CustomerSchema } from './schemas';

const uri = process.env.DB_URI || '';

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

export const db: Db = client.db();

async function connectDb() {
  try {
    await db.command({ ping: 1 });
    console.log('Pinged your deployment. You successfully connected to MongoDB!');
  } catch {
    await client.close();
  }
}

async function createDbCollection(name: string, schema?: Document) {
  try {
    const collections = await db.listCollections().toArray();
    const collectionExists = collections.some((collection) => collection.name === name);
    if (!collectionExists) {
      await db.createCollection(name, {
        validator: schema,
      });
      console.log(`Collection ${name} is created!`);
    }
  } catch {
    console.log(`Cannot create collection ${name}`);
  }
}

export async function initDb() {
  await connectDb();

  await createDbCollection(Collection.CUSTOMERS, CustomerSchema);
  await createDbCollection(Collection.CUSTOMERS_ANONYMISED, CustomerSchema);
}
