export enum Collection {
  CUSTOMERS = 'customers',
  CUSTOMERS_ANONYMISED = 'customers_anonymised',
}
