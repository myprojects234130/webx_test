export const CustomerSchema = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      firstName: {
        bsonType: 'string',
        description: 'must be a string',
      },
      lastName: {
        bsonType: 'string',
        description: 'must be a string',
      },
      email: {
        bsonType: 'string',
        description: 'must be a string',
      },
      address: {
        bsonType: 'object',
        required: ['line1', 'postcode', 'city', 'state', 'country'],
        properties: {
          line1: {
            bsonType: 'string',
            description: 'must be a string',
          },
          line2: {
            bsonType: 'string',
            description: 'must be a string',
          },
          postcode: {
            bsonType: 'string',
            description: 'must be a string',
          },
          city: {
            bsonType: 'string',
            description: 'must be a string',
          },
          state: {
            bsonType: 'string',
            description: 'must be a string',
          },
          country: {
            bsonType: 'string',
            description: 'must be a string',
          },
        },
      },
      createdAt: {
        bsonType: 'date',
        description: 'must be a date',
      },
    },
  },
};
