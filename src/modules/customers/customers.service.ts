import { faker } from '@faker-js/faker';
import { Collection, ObjectId } from 'mongodb';

import { db } from '@db';
import { Collection as CollectionEnum } from '@db/enums';
import { Customer } from '@types';

export class CustomersService {
  private customersCollection!: Collection;

  constructor() {
    this.customersCollection = db.collection(CollectionEnum.CUSTOMERS);
  }

  generate() {
    return {
      firstName: faker.person.firstName(),
      lastName: faker.person.lastName(),
      email: faker.internet.email(),
      address: {
        line1: faker.location.streetAddress(),
        line2: faker.location.secondaryAddress(),
        postcode: faker.location.zipCode(),
        city: faker.location.city(),
        state: faker.location.state(),
        country: faker.location.country(),
      },
      createdAt: faker.date.recent(),
    };
  }

  getAll() {
    return this.customersCollection.find().toArray();
  }

  async getByPk(_id: ObjectId) {
    return this.customersCollection.findOne<Customer>({ _id });
  }

  async insert(customer: Omit<Customer, '_id'>) {
    await this.customersCollection.insertOne(customer);
  }
}

export const customersService = new CustomersService();
