import crypto from 'crypto';

export function anonymise<T>(object: any, anonFields: string[], path: string = '') {
  Object.keys(object).map((key) => {
    if (typeof object[key] === 'object') {
      return anonymise(object[key], anonFields, path + `${key}.`);
    }

    if (!anonFields.includes(path + key)) {
      return;
    }

    if (key === 'email') {
      const [firstHalf, secondHalf] = object[key].split('@');

      object[key] =
        crypto
          .createHash('sha256')
          .update(firstHalf)
          .digest('base64')
          .replace(/[+=/]/g, '')
          .substring(0, 8) + `@${secondHalf}`;

      return;
    }

    object[key] = crypto
      .createHash('sha256')
      .update(object[key].toString())
      .digest('base64')
      .replace(/[+=/]/g, '')
      .substring(0, 8);
  });

  return object as T;
}
