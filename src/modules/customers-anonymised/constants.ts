export const CUSTOMER_ANON_FIELDS = [
  'firstName',
  'lastName',
  'email',
  'address.line1',
  'address.line2',
  'address.postcode',
];
