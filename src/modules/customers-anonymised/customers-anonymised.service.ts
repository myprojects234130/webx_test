import {
  ChangeStream,
  ChangeStreamDeleteDocument,
  ChangeStreamDocument,
  ChangeStreamInsertDocument,
  ChangeStreamUpdateDocument,
  Collection,
  ObjectId,
} from 'mongodb';

import { db } from '@db';
import { Collection as CollectionEnum } from '@db/enums';
import { Customer } from '@types';
import { pubSub } from '@gql/subscriptions';
import { Trigger } from '@gql/subscriptions/enums';

import { anonymise } from './helpers';
import { CUSTOMER_ANON_FIELDS } from './constants';

class CustomerAnonymisedService {
  private customersAnonymisedCollection!: Collection;
  private customersCollection!: Collection;
  private changeStreamIterator?: ChangeStream;

  constructor() {
    this.customersAnonymisedCollection = db.collection(
      CollectionEnum.CUSTOMERS_ANONYMISED,
    );
    this.customersCollection = db.collection(CollectionEnum.CUSTOMERS);
  }

  async getAll() {
    return this.customersCollection.find().toArray();
  }

  async insert(customer: Omit<Customer, '_id'>) {
    await this.customersAnonymisedCollection.insertOne(customer);
  }

  async update(customer: Customer) {
    await this.customersAnonymisedCollection.updateOne(
      { _id: customer._id },
      { $set: customer },
      { upsert: true },
    );
  }

  async delete(_id: ObjectId) {
    await this.customersAnonymisedCollection.deleteOne({ _id });
  }

  public async listen() {
    this.changeStreamIterator = this.customersCollection.watch();

    while (!this.changeStreamIterator.closed) {
      if (await this.changeStreamIterator.hasNext()) {
        const changes =
          (await this.changeStreamIterator.next()) as ChangeStreamDocument<Document>;

        await this.handleCustomersOperation(changes.operationType, changes);

        const anonCustomers = await this.getAll();

        await pubSub.publish(Trigger.CUSTOMERS_ANONYMISED, { anonCustomers });
      }
    }
  }

  public async stopListening() {
    await this.changeStreamIterator?.close();
  }

  private async handleCustomersOperation(
    operationType: string,
    changes: ChangeStreamDocument<Document>,
  ) {
    switch (operationType) {
      case 'delete':
        return this.delete((changes as ChangeStreamDeleteDocument).documentKey._id);

      case 'insert':
        if (!changes) {
          throw new Error('Something went wrong!');
        }

        const insertChanges = { ...changes } as ChangeStreamInsertDocument;

        const anonymisedCustomer = anonymise<Customer>(
          insertChanges.fullDocument,
          CUSTOMER_ANON_FIELDS,
        );

        return this[operationType](anonymisedCustomer);
      case 'update':
        if (!changes) {
          throw new Error('Something went wrong!');
        }
        const updateChanges = { ...changes } as ChangeStreamUpdateDocument;

        if (updateChanges.updateDescription.updatedFields) {
          const anonymisedCustomer = anonymise<Customer>(
            {
              _id: updateChanges.documentKey._id,
              ...updateChanges.updateDescription.updatedFields,
            },
            CUSTOMER_ANON_FIELDS,
          );

          return this[operationType](anonymisedCustomer);
        }

        return;
      default:
        return;
    }
  }
}

export const customerAnonymisedService = new CustomerAnonymisedService();
