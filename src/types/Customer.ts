import { ObjectId } from 'mongodb';

import { CustomerAddress } from './CustomerAddress';

export interface Customer {
  _id: ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  address: CustomerAddress;
  createdAt: Date;
  [key: string]: any;
}
